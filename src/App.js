import axios from 'axios';
import { useState, useEffect } from "react";
import Chart from "chart.js";


// transforma o dado da api no formato que a biblioteca de grafico espera
function parseData(data) {
    const escolas = JSON.parse(data).escolas;

    const dataParsed = {};

    for (let index = 0; index < escolas.length; index++) {
        const element = escolas[index];
        const gerencia = element['Gerência Regional'];

        // ja visitou uma gerencia desse tipo
        if (dataParsed[gerencia]) {
            // soma com o que ja tem
            dataParsed[gerencia] = {
                value: dataParsed[gerencia]['value'] + parseInt(element['Matrículas*']),
            }
        }

        // primaira vez com essa gerencia
        else {
            // inicia com o valor inical
            dataParsed[gerencia] = {
                value: parseInt(element['Matrículas*']),

            }
        }

    }

    return dataParsed;
}

function App() {

    const [apiResponse, setAPIResponse] = useState(JSON.stringify({escolas: []}));

    // quando a variavel que guarda os dados da api forem preechidos chama esse bagulho aqui
    useEffect(() => {
        const dataParsed = parseData(apiResponse)
        var ctx = document.getElementById('pamonha').getContext('2d');
        var myChart = new Chart(ctx, {
            type: 'bar',
            data: {
                labels: Object.keys(dataParsed),

                datasets: [{
                    // label: 'Numero de matriculas',
                    backgroundColor: Object.keys(dataParsed).map(data => 'rgba(56, 209, 97, 0.5)'),
                    borderColor: Object.keys(dataParsed).map(data => 'rgba(56, 209, 97, 0)'),
                    data: Object.values(dataParsed).map(data => data.value),
                    borderWidth: 1
                }]

            },
            options: {
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero: true
                        }
                    }]
                }
            }
        });
    }, [apiResponse])


    // pegar os dados iniciais da API
    useEffect(() => {
        axios.get('https://apisaber.herokuapp.com/escolas').then(response => {
            console.log('response', response);

            setAPIResponse(JSON.stringify(response.data))

        })
    }, [])




    return (
        <div className="App">

            <canvas id="pamonha" width="400" height="400"></canvas>
            {/* <span>{apiResponse}</span> */}


        </div>
    );
}

export default App;
